from django.apps import AppConfig


class TouploadConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'toupload'
